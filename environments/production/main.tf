terraform {
  backend "consul" {
    path = "terraform/gitlab-cicd-infrastructure"
  }

  required_providers {
    nomad = {
      source = "hashicorp/nomad"
      version = "1.4.15"
    }

    vault = {
      source = "hashicorp/vault"
      version = "2.24.1"
    }
  }
}

provider "nomad" {}
provider "vault" {}
