/*
    CICD AppRole
    Used to authenticate CICD workloads
*/
resource "vault_auth_backend" "cicd" {
  type = "approle"
  description = "Authentication for workloads operating in the CICD context"

  path = "cicd"

  local = false

  tune {
    default_lease_ttl = "1h"
    max_lease_ttl  = "720h"
    token_type = "service"
  }
}

/*
    GitLab JWT
    Used to authenticate JWT tokens generated from GitLab to allow pipelines access to vault.

    Any roles generated from this should always be limited by namespace or project otherwise any
    token generated from the instance will be authenticated (in this case all of gitlab.com).

    This is currently configured to use the initial setup (V1) of the GitLab JWT tokens, there looks
    to be an update comming which will update the bound_issuer and a few other fields which will
    need to be updated at somepoint.
*/
resource "vault_jwt_auth_backend" "gitlab_jwt" {
  description = "JWT authentication for GitLab CICD pipelines"
  path = "gitlab"
  type = "jwt"

  jwks_url = "https://gitlab.com/-/jwks"
  bound_issuer = "gitlab.com"

  provider_config = {}

  tune {
    default_lease_ttl = "5m"
    max_lease_ttl = "10m"

    token_type = "batch"
  }
}
