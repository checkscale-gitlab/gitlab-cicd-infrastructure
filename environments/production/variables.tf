variable "cicd_namespace" {
  type = string
  description = "The Nomad namespace in which CICD jobs will operate in"
  default = "cicd-gitlab"
}

variable "nomad_secret_backend_path" {
  type = string
  description = "Path to the Nomad Secrets Backend without a leading or trailing /"
  default = "nomad"
}
